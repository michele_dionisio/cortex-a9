/*
dal device tree di linux si trovano i registri
intc: interrupt-controller@a01000 {
			compatible = "arm,cortex-a9-gic";
			#interrupt-cells = <3>;
			interrupt-controller;
			reg = <0x00a01000 0x1000>,
			      <0x00a00100 0x100>;
			interrupt-parent = <&intc>;
		};
*/

#define GIC_INTERFACE 0x00a00100
#define GIC_DISTRIBUTOR 0x00a01000


/*
 * IRQ line status.
 *
 * Bits 0-7 are the same as the IRQF_* bits in linux/interrupt.h
 *
 * IRQ_TYPE_NONE		- default, unspecified type
 * IRQ_TYPE_EDGE_RISING		- rising edge triggered
 * IRQ_TYPE_EDGE_FALLING	- falling edge triggered
 * IRQ_TYPE_EDGE_BOTH		- rising and falling edge triggered
 * IRQ_TYPE_LEVEL_HIGH		- high level triggered
 * IRQ_TYPE_LEVEL_LOW		- low level triggered
 * IRQ_TYPE_LEVEL_MASK		- Mask to filter out the level bits
 * IRQ_TYPE_SENSE_MASK		- Mask for all the above bits
 * IRQ_TYPE_DEFAULT		- For use by some PICs to ask irq_set_type
 *				  to setup the HW to a sane default (used
 *                                by irqdomain map() callbacks to synchronize
 *                                the HW state and SW flags for a newly
 *                                allocated descriptor).
 *
 * IRQ_TYPE_PROBE		- Special flag for probing in progress
 *
 * Bits which can be modified via irq_set/clear/modify_status_flags()
 * IRQ_LEVEL			- Interrupt is level type. Will be also
 *				  updated in the code when the above trigger
 *				  bits are modified via irq_set_irq_type()
 * IRQ_PER_CPU			- Mark an interrupt PER_CPU. Will protect
 *				  it from affinity setting
 * IRQ_NOPROBE			- Interrupt cannot be probed by autoprobing
 * IRQ_NOREQUEST		- Interrupt cannot be requested via
 *				  request_irq()
 * IRQ_NOTHREAD			- Interrupt cannot be threaded
 * IRQ_NOAUTOEN			- Interrupt is not automatically enabled in
 *				  request/setup_irq()
 * IRQ_NO_BALANCING		- Interrupt cannot be balanced (affinity set)
 * IRQ_MOVE_PCNTXT		- Interrupt can be migrated from process context
 * IRQ_NESTED_THREAD		- Interrupt nests into another thread
 * IRQ_PER_CPU_DEVID		- Dev_id is a per-cpu variable
 * IRQ_IS_POLLED		- Always polled by another interrupt. Exclude
 *				  it from the spurious interrupt detection
 *				  mechanism and from core side polling.
 * IRQ_DISABLE_UNLAZY		- Disable lazy irq disable
 */

/*
timer@a00600 {
			compatible = "arm,cortex-a9-twd-timer";
			reg = <0x00a00600 0x20>;
			interrupts = <1 13 0xf01>;               
                                                //  IRQ_TYPE_EDGE_RISING	= 0x00000001,            rising edge triggered
                                                //	IRQ_LEVEL		= (1 <<  8),
                                                //  IRQ_PER_CPU		= (1 <<  9),
                                                //  IRQ_NOPROBE		= (1 << 10),
                                                //  IRQ_NOREQUEST		= (1 << 11),
                                                //
			interrupt-parent = <&intc>;
			clocks = <&clks IMX6QDL_CLK_TWD>;
		};
*/
#define TIMER_BASE 0x00a00600
#define TIM01INT_IRQn (1*16 + 13)