
#include "platform.h"

#include "interrupt.h"
#include "smp_twd.h"

#include "cortex_a9.h"

#include <stdio.h>
#include <stdint.h>

/* IO definitions (access restrictions to peripheral registers) */
/**
*/
#ifdef __cplusplus
  #define   __I     volatile             /*!< Defines 'read only' permissions                 */
#else
  #define   __I     volatile const       /*!< Defines 'read only' permissions                 */
#endif
#define     __O     volatile             /*!< Defines 'write only' permissions                */
#define     __IO    volatile             /*!< Defines 'read / write' permissions              */

/** \brief Structure type to access the Private Timer
*/
typedef struct
{
  __IO uint32_t LOAD;            //!< \brief  Offset: 0x000 (R/W) Private Timer Load Register
  __IO uint32_t COUNTER;         //!< \brief  Offset: 0x004 (R/W) Private Timer Counter Register
  __IO uint32_t CONTROL;         //!< \brief  Offset: 0x008 (R/W) Private Timer Control Register
  __IO uint32_t ISR;             //!< \brief  Offset: 0x00C (R/W) Private Timer Interrupt Status Register
  __IO uint32_t reserved[4];
  __IO uint32_t WLOAD;           //!< \brief  Offset: 0x020 (R/W) Watchdog Load Register
  __IO uint32_t WCOUNTER;        //!< \brief  Offset: 0x024 (R/W) Watchdog Counter Register
  __IO uint32_t WCONTROL;        //!< \brief  Offset: 0x028 (R/W) Watchdog Control Register
  __IO uint32_t WISR;            //!< \brief  Offset: 0x02C (R/W) Watchdog Interrupt Status Register
  __IO uint32_t WRESET;          //!< \brief  Offset: 0x030 (R/W) Watchdog Reset Status Register
  __O uint32_t WDISABLE;        //!< \brief  Offset: 0x034 ( /W) Watchdog Disable Register
} __attribute__((packed)) Private_Timer_Type;

#define PTIM ((Private_Timer_Type *) TIMER_BASE )   /*!< \brief Timer register struct */


/** \brief Set the load value to timers LOAD register.
* \param [in] value The load value to be set.
*/
static inline void PTIM_SetLoadValue(uint32_t value)
{
  PTIM->LOAD = value;
}

/** \brief Get the load value from timers LOAD register.
* \return Private_Timer_Type::LOAD
*/
static inline uint32_t PTIM_GetLoadValue(void)
{
  return(PTIM->LOAD);
}

/** \brief Set current counter value from its COUNTER register.
*/
static inline void PTIM_SetCurrentValue(uint32_t value)
{
  PTIM->COUNTER = value;
}

/** \brief Get current counter value from timers COUNTER register.
* \result Private_Timer_Type::COUNTER
*/
static inline uint32_t PTIM_GetCurrentValue(void)
{
  return(PTIM->COUNTER);
}

/** \brief Configure the timer using its CONTROL register.
* \param [in] value The new configuration value to be set.
*/
static inline void PTIM_SetControl(uint32_t value)
{
  PTIM->CONTROL = value;
}

/** ref Private_Timer_Type::CONTROL Get the current timer configuration from its CONTROL register.
* \return Private_Timer_Type::CONTROL
*/
static inline uint32_t PTIM_GetControl(void)
{
  return(PTIM->CONTROL);
}

/** ref Private_Timer_Type::CONTROL Get the event flag in timers ISR register.
* \return 0 - flag is not set, 1- flag is set
*/
static inline uint32_t PTIM_GetEventFlag(void)
{
  return (PTIM->ISR & 1UL);
}

/** ref Private_Timer_Type::CONTROL Clears the event flag in timers ISR register.
*/
static inline void PTIM_ClearEventFlag(void)
{
  PTIM->ISR = 1;
}

//////////////////////////////////////////7

volatile static uint32_t counter;

void timer_handler(void) {
  printf("counter is: %lu (%u) (cpuid:%u)\n", counter++, (unsigned int)PTIM_GetLoadValue(), CPUID());
  PTIM_ClearEventFlag();
}

void timer_twd_init(void) {
  counter = 0;
  PTIM_SetLoadValue(100000000L);
                      
  PTIM_SetControl(    ((256 & 0xff) << 8 /* Prescaler */) +
                      (1 << 2 /* IRQ enable */) +
                      (1 << 1 /* Auto reload */) + 
                      (1 << 0  /* enable */) );

  GIC_SetLevelModel(TIM01INT_IRQn, 1, 0);
  install_isr(TIM01INT_IRQn, timer_handler);
  enable_irq(TIM01INT_IRQn);
}