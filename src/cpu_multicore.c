// i.mx6 only

#include "cpu_multicore.h"
#include <stdint.h>

/*
			src: src@20d8000 {
				compatible = "fsl,imx6q-src", "fsl,imx51-src";
				reg = <0x020d8000 0x4000>;
				interrupts = <0 91 IRQ_TYPE_LEVEL_HIGH>,
					     <0 96 IRQ_TYPE_LEVEL_HIGH>;
				#reset-cells = <1>;
			};
*/

#define REGS_SRC_BASE (0x020d8000) //!< Base address for SRC.

/*!
 * @brief HW_SRC_SCR - SRC Control Register (RW)
 *
 * Reset value: 0x00000521
 *
 * The Reset control register (SCR), contains bits that control operation of the reset controller.
 */
typedef union _hw_src_scr {
    volatile uint32_t U;
    volatile uint32_t B;  /*
                            unsigned WARM_RESET_ENABLE : 1; //!< [0] WARM reset enable bit.
                            unsigned SW_GPU_RST : 1; //!< [1] Software reset for gpu
                            unsigned SW_VPU_RST : 1; //!< [2] Software reset for vpu
                            unsigned SW_IPU1_RST : 1; //!< [3] Software reset for ipu1
                            unsigned SW_OPEN_VG_RST : 1; //!< [4] Software reset for open_vg
                            unsigned WARM_RST_BYPASS_COUNT : 2; //!< [6:5] Defines the ckil cycles to count before bypassing the MMDC acknowledge for WARM reset.
                            unsigned MASK_WDOG_RST : 4; //!< [10:7] Mask wdog_rst_b source.
                            unsigned EIM_RST : 1; //!< [11] EIM reset is needed in order to reconfigure the eim chip select.
                            unsigned SW_IPU2_RST : 1; //!< [12] Software reset for ipu2
                            unsigned CORE0_RST : 1; //!< [13] Software reset for core0 only.
                            unsigned CORE1_RST : 1; //!< [14] Software reset for core1 only.
                            unsigned CORE2_RST : 1; //!< [15] Software reset for core2 only.
                            unsigned CORE3_RST : 1; //!< [16] Software reset for core3 only.
                            unsigned CORE0_DBG_RST : 1; //!< [17] Software reset for core0 debug only.
                            unsigned CORE1_DBG_RST : 1; //!< [18] Software reset for core1 debug only.
                            unsigned CORE2_DBG_RST : 1; //!< [19] Software reset for core2 debug only.
                            unsigned CORE3_DBG_RST : 1; //!< [20] Software reset for core3 debug only.
                            unsigned CORES_DBG_RST : 1; //!< [21] Software reset for debug of arm platform only.
                            unsigned CORE1_ENABLE : 1; //!< [22] CPU core1 enable.
                            unsigned CORE2_ENABLE : 1; //!< [23] CPU core2 enable.
                            unsigned CORE3_ENABLE : 1; //!< [24] CPU core3 enable.
                            unsigned DBG_RST_MSK_PG : 1; //!< [25] Do not assert debug resets after power gating event of cpu
                            unsigned RESERVED0 : 6; //!< [31:26] Reserved
                           */
} __attribute__((packed)) hw_src_scr_t;

#define HW_SRC_SCR_ADDR      (REGS_SRC_BASE + 0x0)
#define HW_SRC_SCR           (*(volatile hw_src_scr_t *) HW_SRC_SCR_ADDR)

void cpu_start_secondary(uint8_t coreNumber, cpu_entry_point_t entryPoint, int stopme)
{
    if ((coreNumber == 0) || (coreNumber >= 4)) {
        return;
    }
    volatile uint32_t* gprx = (volatile uint32_t*) (REGS_SRC_BASE + 0x20 + sizeof(uint32_t)*(coreNumber * 2));

    *gprx = (uint32_t) (entryPoint);  // reset handler
    gprx++;
    *gprx = (uint32_t) (0);  // function call after reset handler
    /* iMX6_Firmware_Guide.pdf parag 3.4.1 */

    //HW_SRC_SCR.B |= (1 << (13 + coreNumber));  // core Reset
    HW_SRC_SCR.B |= (1 << (22 + coreNumber - 1));  // core Enable
    
    if (stopme) {
        while(1) { 
            asm volatile ("wfi"::); // sleep arm core
        }
    }
}

void cpu_disable(uint8_t coreNumber)
{    
    if ((coreNumber == 0) || (coreNumber >= 4)) {
        return;
    }
    HW_SRC_SCR.B &= ~(1 << (22 + coreNumber - 1));  // disable core Enable
}
