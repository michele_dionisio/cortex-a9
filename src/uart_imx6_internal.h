/*
 * Copyright (C) 2008, Freescale Semiconductor, Inc. All Rights Reserved
 * THIS SOURCE CODE IS CONFIDENTIAL AND PROPRIETARY AND MAY NOT
 * BE USED OR DISTRIBUTED WITHOUT THE WRITTEN PERMISSION OF
 * Freescale Semiconductor, Inc.
*/
 
/*  
Revision History:
            Modification    Tracking
Author      Date            Number          Description of Changes
---------   ------------    -------------   ----------------------

*/

/*!
 * @file uart.h
 * @brief   various defines used by uart.c
 *
 * @ingroup diag_util
 */

#ifndef __UART_H
#define __UART_H

#ifndef REGS_UART_BASE
#define HW_UART_INSTANCE_COUNT (5) //!< Number of instances of the UART module.
#define HW_UART1 (1) //!< Instance number for UART1.
#define HW_UART2 (2) //!< Instance number for UART2.
#define HW_UART3 (3) //!< Instance number for UART3.
#define HW_UART4 (4) //!< Instance number for UART4.
#define HW_UART5 (5) //!< Instance number for UART5.
#define REGS_UART1_BASE (0x02020000) //!< Base address for UART instance number 1.
#define REGS_UART2_BASE (0x021e8000) //!< Base address for UART instance number 2.
#define REGS_UART3_BASE (0x021ec000) //!< Base address for UART instance number 3.
#define REGS_UART4_BASE (0x021f0000) //!< Base address for UART instance number 4.
#define REGS_UART5_BASE (0x021f4000) //!< Base address for UART instance number 5.

//! @brief Get the base address of UART by instance number.
//! @param x UART instance number, from 1 through 5.
#define REGS_UART_BASE(x) ( (x) == HW_UART1 ? REGS_UART1_BASE : (x) == HW_UART2 ? REGS_UART2_BASE : (x) == HW_UART3 ? REGS_UART3_BASE : (x) == HW_UART4 ? REGS_UART4_BASE : (x) == HW_UART5 ? REGS_UART5_BASE : 0x00d00000)

//! @brief Get the instance number given a base address.
//! @param b Base address for an instance of UART.
#define REGS_UART_INSTANCE(b) ( (b) == REGS_UART1_BASE ? HW_UART1 : (b) == REGS_UART2_BASE ? HW_UART2 : (b) == REGS_UART3_BASE ? HW_UART3 : (b) == REGS_UART4_BASE ? HW_UART4 : (b) == REGS_UART5_BASE ? HW_UART5 : 0)
#endif

#define HW_UART_URXD_ADDR(x)      (REGS_UART_BASE(x) + 0x0)
#define HW_UART_UTXD_ADDR(x)      (REGS_UART_BASE(x) + 0x40)
#define HW_UART_UCR1_ADDR(x)      (REGS_UART_BASE(x) + 0x80)
#define HW_UART_UCR2_ADDR(x)      (REGS_UART_BASE(x) + 0x84)
#define HW_UART_UCR3_ADDR(x)      (REGS_UART_BASE(x) + 0x88)
#define HW_UART_UCR4_ADDR(x)      (REGS_UART_BASE(x) + 0x8c)
#define HW_UART_UFCR_ADDR(x)      (REGS_UART_BASE(x) + 0x90)
#define HW_UART_USR1_ADDR(x)      (REGS_UART_BASE(x) + 0x94)
#define HW_UART_USR2_ADDR(x)      (REGS_UART_BASE(x) + 0x98)
#define HW_UART_UESC_ADDR(x)      (REGS_UART_BASE(x) + 0x9c)
#define HW_UART_UTIM_ADDR(x)      (REGS_UART_BASE(x) + 0xa0)
#define HW_UART_UBIR_ADDR(x)      (REGS_UART_BASE(x) + 0xa4)
#define HW_UART_UBMR_ADDR(x)      (REGS_UART_BASE(x) + 0xa8)
#define HW_UART_UBRC_ADDR(x)      (REGS_UART_BASE(x) + 0xac)
#define HW_UART_ONEMS_ADDR(x)      (REGS_UART_BASE(x) + 0xb0)
#define HW_UART_UTS_ADDR(x)      (REGS_UART_BASE(x) + 0xb4)
#define HW_UART_UMCR_ADDR(x)      (REGS_UART_BASE(x) + 0xb8)


#define IGNORE_RTS 1
#define WORD8 1 
#define STOP1 1

/*
 * UART Control Register Bit Fields.
 */
#define UART_UCR1_ADEN      (1 << 15)           // Auto dectect interrupt
#define UART_UCR1_ADBR      (1 << 14)           // Auto detect baud rate
#define UART_UCR1_TRDYEN    (1 << 13)           // Transmitter ready interrupt enable
#define UART_UCR1_IDEN      (1 << 12)           // Idle condition interrupt
#define UART_UCR1_RRDYEN    (1 << 9)            // Recv ready interrupt enable
#define UART_UCR1_RDMAEN    (1 << 8)            // Recv ready DMA enable
#define UART_UCR1_IREN      (1 << 7)            // Infrared interface enable
#define UART_UCR1_TXMPTYEN  (1 << 6)            // Transimitter empty interrupt enable
#define UART_UCR1_RTSDEN    (1 << 5)            // RTS delta interrupt enable
#define UART_UCR1_SNDBRK    (1 << 4)            // Send break
#define UART_UCR1_TDMAEN    (1 << 3)            // Transmitter ready DMA enable
#define UART_UCR1_DOZE      (1 << 1)            // Doze
#define UART_UCR1_UARTEN    (1 << 0)            // UART enabled
#define UART_UCR2_ESCI      (1 << 15)           // Escape seq interrupt enable
#define UART_UCR2_IRTS      (1 << 14)           // Ignore RTS pin
#define UART_UCR2_CTSC      (1 << 13)           // CTS pin control
#define UART_UCR2_CTS       (1 << 12)           // Clear to send
#define UART_UCR2_ESCEN     (1 << 11)           // Escape enable
#define UART_UCR2_PREN      (1 << 8)            // Parity enable
#define UART_UCR2_PROE      (1 << 7)            // Parity odd/even
#define UART_UCR2_STPB      (1 << 6)            // Stop
#define UART_UCR2_WS        (1 << 5)            // Word size
#define UART_UCR2_RTSEN     (1 << 4)            // Request to send interrupt enable
#define UART_UCR2_ATEN      (1 << 3)            // Aging timer enable
#define UART_UCR2_TXEN      (1 << 2)            // Transmitter enabled
#define UART_UCR2_RXEN      (1 << 1)            // Receiver enabled
#define UART_UCR2_SRST_     (1 << 0)            // SW reset
#define UART_UCR3_PARERREN  (1 << 12)           // Parity enable
#define UART_UCR3_FRAERREN  (1 << 11)           // Frame error interrupt enable
#define UART_UCR3_ADNIMP    (1 << 7)            // Autobaud detection not improved
#define UART_UCR3_RXDSEN    (1 << 6)            // Receive status interrupt enable
#define UART_UCR3_AIRINTEN  (1 << 5)            // Async IR wake interrupt enable
#define UART_UCR3_AWAKEN    (1 << 4)            // Async wake interrupt enable
#define UART_UCR3_RXDMUXSEL (1 << 2)            // RXD muxed input selected
#define UART_UCR3_INVT      (1 << 1)            // Inverted Infrared transmission
#define UART_UCR3_ACIEN     (1 << 0)            // Autobaud counter interrupt enable
#define UART_UCR4_CTSTL_32  (32 << 10)          // CTS trigger level (32 chars)
#define UART_UCR4_INVR      (1 << 9)            // Inverted infrared reception
#define UART_UCR4_ENIRI     (1 << 8)            // Serial infrared interrupt enable
#define UART_UCR4_WKEN      (1 << 7)            // Wake interrupt enable
#define UART_UCR4_IRSC      (1 << 5)            // IR special case
#define UART_UCR4_LPBYP     (1 << 4)            // Low power bypass
#define UART_UCR4_TCEN      (1 << 3)            // Transmit complete interrupt enable
#define UART_UCR4_BKEN      (1 << 2)            // Break condition interrupt enable
#define UART_UCR4_OREN      (1 << 1)            // Receiver overrun interrupt enable
#define UART_UCR4_DREN      (1 << 0)            // Recv data ready interrupt enable
#define UART_UFCR_RXTL_SHF  0                   // Receiver trigger level shift
#define UART_UFCR_RFDIV_1   (5 << 7)            // Reference freq divider (div 1)
#define UART_UFCR_RFDIV_2   (4 << 7)            // Reference freq divider (div 2)
#define UART_UFCR_RFDIV_3   (3 << 7)            // Reference freq divider (div 3)
#define UART_UFCR_RFDIV_4   (2 << 7)            // Reference freq divider (div 4)
#define UART_UFCR_RFDIV_5   (1 << 7)            // Reference freq divider (div 5)
#define UART_UFCR_RFDIV_6   (0 << 7)            // Reference freq divider (div 6)
#define UART_UFCR_RFDIV_7   (6 << 7)            // Reference freq divider (div 7)
#define UART_UFCR_TXTL_SHF  10                  // Transmitter trigger level shift
#define UART_USR1_PARITYERR (1 << 15)           // Parity error interrupt flag
#define UART_USR1_RTSS      (1 << 14)           // RTS pin status
#define UART_USR1_TRDY      (1 << 13)           // Transmitter ready interrupt/dma flag
#define UART_USR1_RTSD      (1 << 12)           // RTS delta
#define UART_USR1_ESCF      (1 << 11)           // Escape seq interrupt flag
#define UART_USR1_FRAMERR   (1 << 10)           // Frame error interrupt flag
#define UART_USR1_RRDY      (1 << 9)            // Receiver ready interrupt/dma flag
#define UART_USR1_AGTIM     (1 << 8)            // Aging timeout interrupt status
#define UART_USR1_RXDS      (1 << 6)            // Receiver idle interrupt flag
#define UART_USR1_AIRINT    (1 << 5)            // Async IR wake interrupt flag
#define UART_USR1_AWAKE     (1 << 4)            // Aysnc wake interrupt flag
#define UART_USR2_ADET      (1 << 15)           // Auto baud rate detect complete
#define UART_USR2_TXFE      (1 << 14)           // Transmit buffer FIFO empty
#define UART_USR2_IDLE      (1 << 12)           // Idle condition
#define UART_USR2_ACST      (1 << 11)           // Autobaud counter stopped
#define UART_USR2_IRINT     (1 << 8)            // Serial infrared interrupt flag
#define UART_USR2_WAKE      (1 << 7)            // Wake
#define UART_USR2_RTSF      (1 << 4)            // RTS edge interrupt flag
#define UART_USR2_TXDC      (1 << 3)            // Transmitter complete
#define UART_USR2_BRCD      (1 << 2)            // Break condition
#define UART_USR2_ORE       (1 << 1)            // Overrun error
#define UART_USR2_RDR       (1 << 0)            // Recv data ready
#define UART_UTS_FRCPERR    (1 << 13)           // Force parity error
#define UART_UTS_LOOP       (1 << 12)           // Loop tx and rx
#define UART_UTS_TXEMPTY    (1 << 6)            // TxFIFO empty
#define UART_UTS_RXEMPTY    (1 << 5)            // RxFIFO empty
#define UART_UTS_TXFULL     (1 << 4)            // TxFIFO full
#define UART_UTS_RXFULL     (1 << 3)            // RxFIFO full
#define UART_UTS_SOFTRST    (1 << 0)            // Software reset


#endif //__UART_H
