/**************************************************************************//**
 * @file     gic.c
 * @brief    Implementation of GIC functions declared in CMSIS Cortex-A9 Core Peripheral Access Layer Header File
 * @version
 * @date     19 Sept 2013
 *
 * @note
 *
 ******************************************************************************/
/* Copyright (c) 2011 - 2013 ARM LIMITED

   All rights reserved.
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
   - Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   - Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
   - Neither the name of ARM nor the names of its contributors may be used
     to endorse or promote products derived from this software without
     specific prior written permission.
   *
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
   ---------------------------------------------------------------------------*/

#include "gic.h"


// #define DEBUG_GIC 

#ifdef DEBUG_GIC
#include <stdio.h>
#define DPRINTF(fmt, ...) do {                                          \
            printf("%s: " fmt, __func__, ## __VA_ARGS__);      \
    } while (0)
#else
#define DPRINTF(fmt, ...)
#endif


/* IO definitions (access restrictions to peripheral registers) */
/**
*/
#ifdef __cplusplus
  #define   __I     volatile             /*!< Defines 'read only' permissions                 */
#else
  #define   __I     volatile const       /*!< Defines 'read only' permissions                 */
#endif
#define     __O     volatile             /*!< Defines 'write only' permissions                */
#define     __IO    volatile             /*!< Defines 'read / write' permissions              */

/** \brief  Structure type to access the Generic Interrupt Controller Distributor (GICD)
 */
typedef struct
{
  __IO uint32_t ICDDCR;
  __I  uint32_t ICDICTR;
  __I  uint32_t ICDIIDR;
       uint32_t RESERVED0[29];
  __IO uint32_t ICDISR[32];
  __IO uint32_t ICDISER[32];
  __IO uint32_t ICDICER[32];
  __IO uint32_t ICDISPR[32];
  __IO uint32_t ICDICPR[32];
  __I  uint32_t ICDABR[32];
       uint32_t RESERVED1[32];
  __IO uint32_t ICDIPR[256];
  __IO uint32_t ICDIPTR[256];
  __IO uint32_t ICDICFR[64];
       uint32_t RESERVED2[128];
  __IO uint32_t ICDSGIR;
} __attribute__((packed)) GICDistributor_Type;

/** \brief  Structure type to access the Controller Interface (GICC)
 */
typedef struct
{
  __IO uint32_t ICCICR;          // +0x000 - RW - CPU Interface Control Register
  __IO uint32_t ICCPMR;          // +0x004 - RW - Interrupt Priority Mask Register
  __IO uint32_t ICCBPR;          // +0x008 - RW - Binary Point Register
  __I  uint32_t ICCIAR;          // +0x00C - RO - Interrupt Acknowledge Register
  __IO uint32_t ICCEOIR;         // +0x010 - WO - End of Interrupt Register
  __I  uint32_t ICCRPR;          // +0x014 - RO - Running Priority Register
  __I  uint32_t ICCHPIR;         // +0x018 - RO - Highest Pending Interrupt Register
  __IO uint32_t ICCABPR;         // +0x01C - RW - Aliased Binary Point Register

       uint32_t RESERVED[55];

  __I  uint32_t ICCIIDR;         // +0x0FC - RO - CPU Interface Identification Register
} __attribute__((packed))  GICInterface_Type;

/*@} end of GICD */


#define GICDistributor      ((GICDistributor_Type      *)     GIC_DISTRIBUTOR ) /*!< GIC Distributor configuration struct */
#define GICInterface        ((GICInterface_Type        *)     GIC_INTERFACE )   /*!< GIC Interface configuration struct */

void GIC_EnableDistributor(void)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDDCR |= 1; //enable distributor
}

void GIC_DisableDistributor(void)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDDCR &=~1; //disable distributor
}

uint32_t GIC_DistributorInfo(void)
{
    DPRINTF("%s\n", __func__);
    return (uint32_t)(GICDistributor->ICDICTR);
}

uint32_t GIC_DistributorImplementer(void)
{
    DPRINTF("%s\n", __func__);
    return (uint32_t)(GICDistributor->ICDIIDR);
}

void GIC_SetTarget(IRQn_Type IRQn, uint32_t cpu_target)
{
    DPRINTF("%s\n", __func__);
    volatile uint8_t* field = (volatile uint8_t*)&(GICDistributor->ICDIPTR[IRQn / 4]);
    field += IRQn % 4;
    *field = (uint8_t)cpu_target & 0xf;
    DPRINTF("%s(0x%x)\n", __func__, (unsigned int)cpu_target & 0xf);
}

void GIC_SetICDICFR (const uint32_t *ICDICFRn)
{
    DPRINTF("%s\n", __func__);
    uint32_t i, num_irq;

    //Get the maximum number of interrupts that the GIC supports
    num_irq = 32 * ((GIC_DistributorInfo() & 0x1f) + 1);

    for (i = 0; i < (num_irq/16); i++)
    {
        GICDistributor->ICDISPR[i] = *ICDICFRn++;
    }
}

uint32_t GIC_GetTarget(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    volatile uint8_t* field = (volatile uint8_t*)&(GICDistributor->ICDIPTR[IRQn / 4]);
    field += IRQn % 4;
    return ((uint32_t)*field & 0xf);
}

void GIC_EnableInterface(void)
{
    DPRINTF("%s\n", __func__);
    GICInterface->ICCICR |= (1 << 0) | (1 << 1); //enable interface
}

void GIC_DisableInterface(void)
{
    DPRINTF("%s\n", __func__);
    GICInterface->ICCICR &= ~((1 << 0) | (1 << 1)); //disable distributor
}

IRQn_Type GIC_AcknowledgePending(void)
{
    DPRINTF("%s\n", __func__);
    return (IRQn_Type)(GICInterface->ICCIAR);
}

void GIC_EndInterrupt(IRQn_Type IRQn)
{
    DPRINTF("%s(%d)\n", __func__, IRQn);
    GICInterface->ICCEOIR = IRQn;
}

void GIC_EnableIRQ(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDISER[IRQn / 32] = 1 << (IRQn % 32);
}

void GIC_DisableIRQ(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDICER[IRQn / 32] = 1 << (IRQn % 32);
}

void GIC_SetPendingIRQ(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDISPR[IRQn / 32] = 1 << (IRQn % 32);
}

void GIC_ClearPendingIRQ(IRQn_Type IRQn)
{
    DPRINTF("%s(%d)\n", __func__, IRQn);
    GICDistributor->ICDICPR[IRQn / 32] = 1 << (IRQn % 32);
}

void GIC_SetLevelModel(IRQn_Type IRQn, int8_t edge_level, int8_t model)
{
    DPRINTF("%s\n", __func__);
    volatile uint8_t* field = (volatile uint8_t*)&(GICDistributor->ICDICFR[IRQn / 16]);
    int  bit_shift = (IRQn % 16)<<1;
    uint8_t save_byte;

    field += (bit_shift / 8);
    bit_shift %= 8;

    save_byte = *field;
    save_byte &= ((uint8_t)~(3u << bit_shift));

    *field = save_byte | ((uint8_t)((edge_level<<1) | model)<< bit_shift);
}

void GIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
    DPRINTF("%s\n", __func__);
    volatile uint8_t* field = (volatile uint8_t*)&(GICDistributor->ICDIPR[IRQn / 4]);
    field += (IRQn % 4);
    *field = (uint8_t)priority;
}

uint32_t GIC_GetPriority(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    volatile uint8_t* field = (volatile uint8_t*)&(GICDistributor->ICDIPR[IRQn / 4]);
    field += (IRQn % 4);
    return (uint32_t)*field;
}

void GIC_InterfacePriorityMask(uint32_t priority)
{
    DPRINTF("%s\n", __func__);
    GICInterface->ICCPMR = priority & 0xff; //set priority mask
}

void GIC_SetBinaryPoint(uint32_t binary_point)
{
    DPRINTF("%s\n", __func__);
    GICInterface->ICCBPR = binary_point & 0x07; //set binary point
}

uint32_t GIC_GetBinaryPoint(uint32_t binary_point)
{
    DPRINTF("%s\n", __func__);
    return (uint32_t)GICInterface->ICCBPR;
}

uint32_t GIC_GetIRQStatus(IRQn_Type IRQn)
{
    DPRINTF("%s\n", __func__);
    uint32_t pending, active;

    active = ((GICDistributor->ICDABR[IRQn / 32])  >> (IRQn % 32)) & 0x1;
    pending =((GICDistributor->ICDISPR[IRQn / 32]) >> (IRQn % 32)) & 0x1;

    return ((active<<1) | pending);
}

void GIC_SendSGI(IRQn_Type IRQn, uint32_t target_list, uint32_t filter_list)
{
    DPRINTF("%s\n", __func__);
    GICDistributor->ICDSGIR = ((filter_list & 0x3) << 24) | ((target_list & 0xff) << 16) | (IRQn & 0xf);
}

void GIC_DistInit()
{
    DPRINTF("%s\n", __func__);
    //A reset sets all bits in the ICDISRs corresponding to the SPIs to 0,
    //configuring all of the interrupts as Secure.

    //Disable interrupt forwarding
    GIC_DisableDistributor();
    //Get the maximum number of interrupts that the GIC supports
    uint32_t num_irq = 32 * ((GIC_DistributorInfo() & 0x1f) + 1);

    /* Priority level is implementation defined.
     To determine the number of priority bits implemented write 0xFF to an ICDIPR
     priority field and read back the value stored.*/
    GIC_SetPriority((IRQn_Type)0, 0xff);
    uint32_t priority_field = GIC_GetPriority((IRQn_Type)0);

    for (int i = 32; i < num_irq; i++)
    {
        //Disable all SPI the interrupts
        GIC_DisableIRQ((IRQn_Type)i);
        //Set level-sensitive and N-N model
        GIC_SetLevelModel(i, 0, 0);
        //Set priority
        GIC_SetPriority((IRQn_Type)i, priority_field/2);
        //Set target list to USECORE
        GIC_SetTarget((IRQn_Type)i, USECORE);
    }
    
    //Enable distributor
    GIC_EnableDistributor();
}

void GIC_CPUInterfaceInit(void)
{
    DPRINTF("%s\n", __func__);
    IRQn_Type i;
    uint32_t priority_field;

    //A reset sets all bits in the ICDISRs corresponding to the SPIs to 0,
    //configuring all of the interrupts as Secure.

    //Disable interrupt forwarding
    GIC_DisableInterface();

    /* Priority level is implementation defined.
     To determine the number of priority bits implemented write 0xFF to an ICDIPR
     priority field and read back the value stored.*/
    GIC_SetPriority((IRQn_Type)0, 0xff);
    priority_field = GIC_GetPriority((IRQn_Type)0);

    //SGI and PPI
    for (i = (IRQn_Type)0; i < 32; i++)
    {
        //Set level-sensitive and N-N model for PPI
        //if(i > 15)
            //GIC_SetLevelModel(i, 0, 0);
        //Disable SGI and PPI interrupts
        GIC_DisableIRQ(i);
        //Set priority
        GIC_SetPriority(i, priority_field/2);
    }
    //Enable interface
    GIC_EnableInterface();
    //Set binary point to 0
    GIC_SetBinaryPoint(0);
    //Set priority mask
    GIC_InterfacePriorityMask(0xff);
}

void GIC_Enable()
{
    DPRINTF("%s\n", __func__);
    GIC_DistInit();
    GIC_CPUInterfaceInit(); //per CPU
}