#include <stdint.h>

/*
	http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0388e/CIHEBGFG.html

	MRC p15,0,<Rd>,c0,c0,5; read Multiprocessor ID register

	Bits	Name	Description
	[31]	-	Indicates the register uses the new multiprocessor format. This is always 1.
	[30]	U bit	
		0 = Processor is part of a multiprocessor system.

		1 = Processor is part of a uniprocessor system.

	[29:12]	-	SBZ.
	[11:8]	Cluster ID	
		Value read in CLUSTERID configuration pins. It identifies a Cortex-A9 MPCore processor in a system with more than one Cortex-A9 MPCore processor present. SBZ in a uniprocessor configuration.

	[7:2]	-	SBZ.
	[1:0]	CPU ID	
		The value depends on the number of configured processors.
		One Cortex-A9 processor, the CPU ID is 0x0.
		Two Cortex-A9 processors, the CPU IDs are 0x0 and 0x1.
		Three Cortex-A9 processors, the CPU IDs are 0x0, 0x1, and 0x2.
		Four Cortex-A9 processors, the CPU IDs are 0x0, 0x1, 0x2,and 0x3.

*/

static inline unsigned int cortex_a9_cpu_id(void) {
	uint32_t val;
	asm volatile (" mrc p15,0,%[val],c0,c0,5\n":[val] "=r" (val)::);
	return (val & 0x3);
}

#define CPUID() cortex_a9_cpu_id()


void arm_branch_target_cache_invalidate_is(void);
void arm_branch_target_cache_invalidate(void);
void arm_branch_prediction_disable(void);
void arm_branch_prediction_enable(void);

#if 0

/**
 * Wait N CPU cycles (ARM CPU only)
 */
static inline void wait_cycles(unsigned int n)
{
    if(n) while(n--) { asm volatile("nop"); }
}

#endif