#include "interrupt.h"
#include "cortex_a9.h"
#include <stdio.h>

static func_t isr_table[MAXIRQNUM];

void install_isr(IRQn_Type irq_num, func_t handler) {
	isr_table[irq_num] = handler;
}

// IRQ, FIQ, SWI, ABORT and UNDEF.
void __attribute__ ((interrupt("UNDEF"))) c_undef(void) {
	printf("yaay UNDEF ticked\n");
}
void __attribute__ ((interrupt("ABORT"))) c_abort_data(void) {
	printf("yaay ABORT DATA ticked\n");
}
void __attribute__ ((interrupt("ABORT"))) c_abort_instruction(void) {
	printf("yaay ABORT INSTRUCTION ticked\n");
}
void __attribute__ ((interrupt("SWI"))) c_svc(void) {
	uint32_t num;
	asm volatile ("ldr %[num],[lr,#-4]":[num] "=r" (num)::);
	num &= 0x00FFFFFF;
	printf("yaay SVC ticked: %u\n", (unsigned int)num);
}
void __attribute__ ((interrupt("FIQ"))) c_fiq(void) {
	printf("yaay FIQ ticked\n");
}
void __attribute__ ((interrupt("IRQ"))) c_irq(void) {
	asm volatile("cpsid i" : : : "memory", "cc");
	int irq_num = GIC_AcknowledgePending();
	GIC_ClearPendingIRQ(irq_num);
	if (irq_num < MAXIRQNUM) {
		if(isr_table[irq_num] != NULL){
			isr_table[irq_num]();
		} else{
			printf("no handler found for %d\n", irq_num);
		}
	} else {
		printf("interrupt too big %d\n", irq_num);
	}
	
	GIC_EndInterrupt(irq_num);
	asm volatile("cpsie i" : : : "memory", "cc");
}

void enable_irq(IRQn_Type irq_num) {
	GIC_EnableIRQ(irq_num);
}
void interrupt_init(void) {
	GIC_Enable();
	asm volatile("cpsie i" : : : "memory", "cc");
}
