
#include <stdint.h>
#include "cortex_a9.h"

#define _ARM_MRC(coproc, opcode1, Rt, CRn, CRm, opcode2)	\
    asm volatile ("mrc p" #coproc ", " #opcode1 ", %[output], c" #CRn ", c" #CRm ", " #opcode2 "\n" : [output] "=r" (Rt))
    
#define _ARM_MCR(coproc, opcode1, Rt, CRn, CRm, opcode2)	\
    asm volatile ("mcr p" #coproc ", " #opcode1 ", %[input], c" #CRn ", c" #CRm ", " #opcode2 "\n" :: [input] "r" (Rt))

void arm_branch_prediction_enable(void) {
    uint32_t val;   
	_ARM_MRC(15, 0, val, 1, 0, 0);    // @ Read SCTLR
    val |= (1 << 11);                 // @ Set the Z bit (bit 11)
    _ARM_MCR(15, 0, val, 1, 0, 0);    // @ Write SCTLR
}

void arm_branch_prediction_disable(void) {
    uint32_t val;
    _ARM_MRC(15, 0, val, 1, 0, 0);    // @ Read SCTLR
    val &= ~(1 << 11);                                          // @ Clear the Z bit (bit 11)
    _ARM_MCR(15, 0, val, 1, 0, 0);    // @ Write SCTLR
}

void arm_branch_target_cache_invalidate(void) {
    uint32_t val = 0;
    _ARM_MCR(15, 0, val, 7, 5, 6);    // @ BPIALL - Invalidate entire branch predictor array
}

void arm_branch_target_cache_invalidate_is(void) {
    uint32_t val = 0;
    _ARM_MCR(15, 0, val, 7, 1, 6);    // @ BPIALLIS - Invalidate entire branch predictor array Inner Shareable
}
