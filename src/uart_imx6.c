#include <stdint.h>
#include "uart_imx6_internal.h"

#define BIT13 0x02000

void uart_putc (char c) {
    while ( !((*(volatile uint32_t*)(HW_UART_USR1_ADDR(HW_UART1))) & BIT13)  );
    (*(volatile uint32_t*)(HW_UART_UTXD_ADDR(HW_UART1))) = c ;
}
