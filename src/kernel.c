#include <stdint.h>
#include <stdio.h>
#include "smp_twd.h"
#include "interrupt.h"

int main(void){
 	interrupt_init();
 	timer_twd_init();
 	asm volatile("SVC 0x05");  // simulate software interrupt
 	for(;;) {
		 asm volatile ("wfi"::); // sleep arm core
	}
	return 0;
}
