#include <stdint.h>

typedef void (*cpu_entry_point_t)(void);

void cpu_start_secondary(uint8_t coreNumber, cpu_entry_point_t entryPoint, int stopme);
void cpu_disable(uint8_t coreNumber);