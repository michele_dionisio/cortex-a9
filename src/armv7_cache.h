#include <stddef.h>

int arm_dcache_state_query();
void arm_dcache_enable();
void arm_dcache_disable();
void arm_dcache_invalidate();
void arm_dcache_invalidate_line(const void * addr);
void arm_dcache_invalidate_mlines(const void * addr, size_t length);
void arm_dcache_flush();
void arm_dcache_flush_line(const void * addr);
void arm_dcache_flush_mlines(const void * addr, size_t length);
int arm_icache_state_query();
void arm_icache_enable();
void arm_icache_disable();
void arm_icache_invalidate();
void arm_icache_invalidate_is();
void arm_icache_invalidate_line(const void * addr);
void arm_icache_invalidate_mlines(const void * addr, size_t length);
