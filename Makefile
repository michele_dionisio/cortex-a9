TOOLCHAIN = arm-none-eabi
TARGET = kernel.elf
TARGET_MAP = kernel.map
TARGET_STRIP = kernel_strip.elf
TARGET_BIN = kernel.bin
ARMARCH = armv7-a
CORE = cortex-a9
CC = $(TOOLCHAIN)-gcc
AS = $(TOOLCHAIN)-as
SIZE = $(TOOLCHAIN)-size
DUMP = $(TOOLCHAIN)-objdump
COPY = $(TOOLCHAIN)-objcopy
STRIP = $(TOOLCHAIN)-strip

USECORE = 1

SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

LFILE = $(SRCDIR)/linker.ld
# Compile flags here
CFLAGS   = -std=gnu11 -Werror -Wall -nostartfiles -fno-exceptions -mcpu=$(CORE) -static -g3 -ggdb -O0 -DUSECORE=$(USECORE) -ffunction-sections -fdata-sections
AFLAGS   = -g3 --defsym USECORE=$(USECORE)
LINKER   = $(CC) -o 
# linking flags here
LFLAGS   = -Wl,--cref,-Map=$(@:.elf=.map) -Wall -T $(LFILE) -nostartfiles -fno-exceptions --specs=nosys.specs -mcpu=$(CORE) -O0 -static -g3 -ggdb -lc -Wl,--gc-sections

GDB = gdb-multiarch
QEMU = qemu-system-arm
QEMU_OPTS = -M sabrelite -cpu cortex-a9 -nographic -smp 4 -serial mon:stdio -kernel


C_FILES := $(wildcard $(SRCDIR)/*.c)
AS_FILES := $(wildcard $(SRCDIR)/*.S)
OBJECTS_C := $(addprefix $(OBJDIR)/,$(notdir $(C_FILES:.c=.o)))
OBJECTS_S := $(addprefix $(OBJDIR)/,$(notdir $(AS_FILES:.S=.o)))
OBJECTS_ALL := $(OBJECTS_S) $(OBJECTS_C)
rm = rm -f

all: $(BINDIR)/$(TARGET) $(BINDIR)/$(TARGET_STRIP) $(BINDIR)/$(TARGET_BIN)


$(BINDIR)/$(TARGET): $(OBJECTS_ALL) $(LFILE)
	@mkdir -p $(@D)
	@echo "Linking: $(LINKER) $@ $(LFLAGS) $(OBJECTS_ALL)"
	@$(LINKER) $@ $(LFLAGS) $(OBJECTS_ALL)
	@echo "Linking complete!"
	@$(SIZE) $@

$(BINDIR)/$(TARGET_STRIP): $(BINDIR)/$(TARGET)
	@$(STRIP) -s -o $@ $<

$(BINDIR)/$(TARGET_BIN): $(BINDIR)/$(TARGET)
	$(COPY) -O binary $< $@


$(OBJECTS_ALL) : | obj

$(OBJDIR):
	@mkdir -p $@

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	@echo "Compiling: $(CC) $(CFLAGS) -c $< -o $@"
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

$(OBJDIR)/%.o : $(SRCDIR)/%.S
	@echo "Assembly: $(AS) $(AFLAGS) $< -o $@"
	@$(AS) $(AFLAGS) $< -o $@
	@echo "Assembled "$<" successfully!"

qemu: $(BINDIR)/$(TARGET_STRIP)
	$(QEMU) $(QEMU_OPTS) $(BINDIR)/$(TARGET_STRIP)

gdb: $(BINDIR)/$(TARGET)
	$(GDB) -ex "target remote tcp:localhost:1234" $(BINDIR)/$(TARGET)

dqemu: $(BINDIR)/$(TARGET)
	$(QEMU) -s -S $(QEMU_OPTS) $(BINDIR)/$(TARGET)

.PHONY: clean
clean:
	@$(rm) $(OBJECTS_ALL)
	@$(rm) $(BINDIR)/$(TARGET)
	@$(rm) $(BINDIR)/$(TARGET_STRIP)
	@$(rm) $(BINDIR)/$(TARGET_BIN)
	@$(rm) $(BINDIR)/$(TARGET_MAP)
	@echo "Cleanup complete!"
